const subscribeMessage = {
    "text": "Do you want to subscribe to Billy?",
    "attachments": [
        {
            "fallback": "Shame... buttons aren't supported in this land",
            "callback_id": "subscribe",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "actions": [
                {
                    "name": "yes",
                    "text": "YES",
                    "type": "button",
                    "value": "yes"
                },
                {
                    "name": "no",
                    "text": "NO",
                    "type": "button",
                    "value": "no"
                }
            ]
        }
    ]
}

module.exports = {
    subscribeMessage,
}