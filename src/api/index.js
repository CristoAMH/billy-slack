const express = require('express')
const commandsController = require('./controllers/commands')
const verificationMiddleware = require('./middleware/verification')

const router = new express.Router()

/* router.use(verificationMiddleware) */
router.use(commandsController)

module.exports = router