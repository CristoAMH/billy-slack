const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    nombre: {
        type: String,
    },
    email: {
        type: String,
    },
    username: String,
    slackUserId: String,
    slackConnected: Boolean,
    password:String,
    publicAWSKey:String,
    privateAWSKey:String,
    createdAt: Number,
    isActive: Boolean,
    subDay: Boolean,
    subMonth: Boolean,
    lastDayCall: Number,
    lastMonthCall: Number,
    token: String
});

const UserModel = mongoose.model('user', UserSchema);


//TEMPORARY
const sendFakeUser = (req, res) => {
    UserModel.insertMany({
        "name" : "Cristo Alberto",
        "username" : "cristoamh",
        "password" : "123",
        "email" : "cristo@medina.com",
        "publicAWSKey" : "",
        "privateAWSKey" : "",
        "slackUserId" : "",
        "slackConnected" : false
    })
    .then(response => res.json(response))
    .catch(err => console.log(err))
}

module.exports = {
    UserModel,
    sendFakeUser
};
