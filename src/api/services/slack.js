//const slack = require('slack')
const axios = require('axios');
const {UserModel} = require('./db')
const {subscribeMessage} = require('../utils/consts.js')


/* module.exports.billyMessage = (channel, text) => slack.chat.postMessage
({token: process.env.slack_app_token, channel, text})
module.exports.pinPostedMessage = postMessageResponse => slack.pins.add
({token: process.env.slack_app_token, channel: postMessageResponse.channel,
     timestamp: postMessageResponse.ts}) */

const logOut = (req, res) => {
    UserModel.findOne({slackUserId : req.body.user_id, slackConnected : true}, (err, user) => {
        if(user === null) {return res.send(`You aren't logged in`)}
        user.slackConnected = false;
        user.save((err, updatedUser) =>{
            if(err){ return err};
            console.log(updatedUser)
        })
        return res.send(`You have been disconnected!`)
    })
}
const logIn = (req, res) => {
    if(req.body.text === ""){ return res.send(`You must insert USERNAME and PASSWORD`)}
    const [username, password] = req.body.text.split(" ")
    let connected = false;
    UserModel.findOne({username, password}, (err, user) => {
        if(user === null){ return res.send(`Wrong USERNAME or PASSWORD, if you don't have an account please register in www.register.com`)}
        user.slackUserId = req.body.user_id;
        user.save((err, updatedUser) =>{
            if(err){ return err};
            console.log(updatedUser)
        })
        if(user.slackConnected !== true){
            user.slackConnected = true;
            connected = true;
            user.save((err, updatedUser) =>{
                if(err){ return err};
                console.log(updatedUser)
            })
        }
        if(connected === true){
            return res.send(`You have been logged perfectly`)
        } else {
            return res.send('You are already logged in')            
        }      
    })
}

const subscribeToBilly = (req, res) => {
    UserModel.findOne({slackUserId : req.body.user_id, slackConnected : true}, (err, user) =>{
        if(user === null){ return res.send(`You must loggin first`)}       
    })
    .then( res => {
        //This is the URL to make the alarm
        const responseURL = req.body.response_url
        sendMessageToSlackResponseURL(responseURL, subscribeMessage)
    })
    .catch( err => {
        console.log(err)
    })
}

const getCosts = (req, res) => {
    const slackUserId = req.body.user_id
    const slackUser = UserModel.find({slackUserId})
    if(slackUser.slackConnected === false){
        return res.send('You have to do "/login" first');
    }
   axios.get('http://localhost:4000/awsapi')
   .then( response => {
       const BillForDay = [];
        const AWSJson = response.data;
        AWSJson.ResultsByTime.forEach(day => {
            const startBill = day.TimePeriod.Start
            const endBill = day.TimePeriod.End
            const amortizedCostAmount = day.Total.AmortizedCost.Amount
            const amortizedCostUnit = day.Total.AmortizedCost.Unit
            const Bill = `Between ${startBill} and ${endBill} you have spent ${amortizedCostAmount}${amortizedCostUnit}`
            BillForDay.push(Bill)
        });
       res.send(BillForDay.join('\n'))
    })
   .catch( err => console.log( 'err -> ', err))
}

const actionsFromInteractiveMessages = (req, res) => {
    console.log('body -> ', req.body)
    console.log('payload ->', req.body.payload)
    var actionJSONPayload = JSON.parse(req.body.payload); // parse URL-encoded payload JSON strin
    if(actionJSONPayload.callback_id === 'subscribe'){
        const message = {
            "text": `You have been suscribed to Billy`,
            "replace_original": false
        }
        sendMessageToSlackResponseURL(actionJSONPayload.response_url, message)
    }  
}

function sendMessageToSlackResponseURL(responseURL, JSONmessage){
    var postOptions = {
        url: responseURL,
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        data: JSONmessage
    }
    axios(postOptions, (error, response, body) => {
        if (error){
            console.log(error)
        }
    })
}

module.exports = {
    logOut,
    logIn,
    subscribeToBilly,
    getCosts,
    actionsFromInteractiveMessages
}