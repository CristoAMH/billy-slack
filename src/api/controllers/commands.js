const express = require('express');
const {sendFakeUser} = require('../services/db')
const {logOut, logIn, subscribeToBilly, getCosts,
actionsFromInteractiveMessages} = require('../services/slack')
//const {billyMessage, pinPostedMessage} = require('../services/slack.js')

const router = new express.Router()


router.post('/commands/login', (req, res) => {
    logIn(req, res);
})

router.post('/commands/logout', (req, res) => {
    logOut(req, res);
})

router.post('/commands/subscribe', async (req, res) => {
    subscribeToBilly(req, res);
})

router.post('/commands/costs', (req,res) => {
    getCosts(req, res);
})
//This respond at every interactive message from slack
router.post('/commands/actions', (req, res) =>{
    actionsFromInteractiveMessages(req, res);
})

//This is just to fake the BD
router.post('/users', (req,res) => {
    sendFakeUser(req, res);
})


module.exports =  router