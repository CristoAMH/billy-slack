const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();


const api = require('./api');

const port = process.env.PORT || 3000;
const app = express(); 

////DB
mongoose.connect(process.env.mongoDbUrl)
mongoose.Promise = global.Promise;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
///
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', api)
app.listen(port, () => console.log(`Escuchando en el puerto: ${port}`))













/* app.get('/', (req, res) => res.json(credentials) )

app.post('/signin', (req, res) => {
    let credentials = (req.body.text).split(" ");
    const [username,password] = [...credentials];
    console.log(username, password)
    res.send(`this is your username: ${username} and this your password: ${password}`);
}) */

