## Instruccions

### MongoDB

1. Docker pull mongo
2. docker run --rm --name mongodb -p 27017:27017 -d mongo

### Billy-back

0. git clone https://gitlab.com/Juancm91/ProyectoBillsAWS.git **(Do this if you haven't done it previously)**
1. npm run dev

### Billy-SlackBot

0. git clone https://gitlab.com/CristoAMH/billy-slack.git **(Do this if you haven't done it previously)**
1. npm run start